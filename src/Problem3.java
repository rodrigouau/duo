import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Problem3 implements Calculo {

	@Override
	public List<CustoCargo> custoPorCargo(List<Funcionario> funcionarios) {

		Set<String> cargos = new HashSet<>();
		funcionarios.forEach(f -> cargos.add(f.getCargo()));
		
		List<CustoCargo> listaCargos = new ArrayList<>();
		for(String cargo:cargos){
			CustoCargo custoCargo = new CustoCargo();
			custoCargo.setCargo(cargo);
			custoCargo.setCusto(new BigDecimal(0.0));
			funcionarios.stream()
				.filter(funcionario -> funcionario.getCargo().equals(cargo))
				.forEach(funcionario -> custoCargo.setCusto(custoCargo.getCusto().add(funcionario.getSalario())));
			listaCargos.add(custoCargo);
		}
		return listaCargos;
	}

	@Override
	public List<CustoDepartamento> custoPorDepartamento(List<Funcionario> funcionarios) {

		Set<String> dpartamentos = new HashSet<>();
		funcionarios.forEach(f -> dpartamentos.add(f.getDepartamento()));
		
		List<CustoDepartamento> listaDepartamentos = new ArrayList<>();
		for(String dept:dpartamentos){
			CustoDepartamento custoDepartamento= new CustoDepartamento();
			custoDepartamento.setDepartamento(dept);
			custoDepartamento.setCusto(new BigDecimal(0.0));
			funcionarios.stream()
				.filter(funcionario -> funcionario.getDepartamento().equals(dept))
				.forEach(funcionario -> custoDepartamento.setCusto(custoDepartamento.getCusto().add(funcionario.getSalario())));
			listaDepartamentos.add(custoDepartamento);
		}
		return listaDepartamentos;
	}
	
	public static void main(String []args){
		
		Funcionario funcionario1 = new Funcionario("Assistente", "Administrativo", new BigDecimal(1000.0));
		Funcionario funcionario2 = new Funcionario("Gerente", "Administrativo", new BigDecimal(7000.70));
		Funcionario funcionario3 = new Funcionario("Diretor", "Administrativo", new BigDecimal(10000.45));
		Funcionario funcionario4 = new Funcionario("Assistente", "Financeiro", new BigDecimal(1300.9));
		Funcionario funcionario5 = new Funcionario("Gerente", "Financeiro", new BigDecimal(7500));
		Funcionario funcionario6 = new Funcionario("Diretor", "Financeiro", new BigDecimal(11000.0));
		Funcionario funcionario7 = new Funcionario("Estagi�rio", "Jur�dico", new BigDecimal(700.4));
		Funcionario funcionario8 = new Funcionario("Assistente", "Jur�dico", new BigDecimal(1800.90));
		Funcionario funcionario9 = new Funcionario("Gerente", "Jur�dico", new BigDecimal(9500.50));
		Funcionario funcionario10 = new Funcionario("Diretor", "Jur�dico", new BigDecimal(13000.0));
		
		List<Funcionario> listaFuncionario = new ArrayList<>();
		listaFuncionario.add(funcionario1);
		listaFuncionario.add(funcionario2);
		listaFuncionario.add(funcionario3);
		listaFuncionario.add(funcionario4);
		listaFuncionario.add(funcionario5);
		listaFuncionario.add(funcionario6);
		listaFuncionario.add(funcionario7);
		listaFuncionario.add(funcionario8);
		listaFuncionario.add(funcionario9);
		listaFuncionario.add(funcionario10);
		
		Problem3 three = new Problem3();
		List<CustoCargo> listaCustoCargo = three.custoPorCargo(listaFuncionario);
		List<CustoDepartamento> listaCustoDepartamento = three.custoPorDepartamento(listaFuncionario);
		
		System.out.println("Custo por cargo:");
		listaCustoCargo.forEach(cc -> System.out.println(cc.getCargo() + " : " + cc.getCusto()));

		System.out.println("Custo por departamento:");
		listaCustoDepartamento.forEach(cd -> System.out.println(cd.getDepartamento() + " : " + cd.getCusto()));
		
	}

}
