
import java.util.Arrays;

public class Problem1 implements FindArray {

	  public static void main(String[] args)
	  {
	    //[4,9,3,7,8] e [3,7] deve retornar 2.
		//[1,3,5] e [1] deve retornar 0.
		//[7,8,9] e [8,9,10] deve retornar -1.
		//[4,9,3,7,8,3,7,1] e [3,7] deve retornar 5.
		
	    int[] array = {4,9,3,7,8,3,7,1};
	    int[] subarray = {3,7};

	    System.out.println(new Problem1().findArray(array, subarray));
	  }

	@Override
	public int findArray(int[] array, int[] subArray) {

	    String arrayAsString = Arrays.toString(array).replaceAll("[^0-9]+", "");
	    String subArrayAsString = Arrays.toString(subArray).replaceAll("[^0-9]+", "");
;
		return arrayAsString.lastIndexOf(subArrayAsString);
	}
}
